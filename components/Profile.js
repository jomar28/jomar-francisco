import { useEffect } from 'react'
import AOS from 'aos'
import 'aos/dist/aos.css';
import { Container, Row, Col } from 'react-bootstrap'
import Image from 'next/image'

export default function Profile() {

      useEffect(() => {
            AOS.init({
                  duration: 1800
            })
      }, [])

      return (
            <Container className="profile">
                  <Row className="p-0">
                        <Col id="name">
                              <p className="font-weight-bold text">developer</p>
                              <p className="font-weight-bold">Jomar Francisco</p>
                              <h2 className="d-flex justify-content-end welcome display-2" data-aos="slide-down">WELCOME</h2>
                              <img src="/me.jpg" className="profile-image d-block mx-auto mt-5"/>
                              <pre className="font-weight-bold text-secondary text-1 text-center mt-2">objective   <span className="fw-normal font-weight-bold">become a digital nomad while working full-time in tech</span></pre>
                        </Col>
                  </Row>                 
            </Container>

      )

}