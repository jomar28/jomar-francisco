import { useEffect } from 'react'
import { Container, Row, Col, Carousel } from 'react-bootstrap'
import AOS from 'aos'
import 'aos/dist/aos.css';

export default function Projects() {

      useEffect(() => {
            AOS.init({
                  // offset: 200,
                  duration: 900
            })
      }, [])

      /*
            First project:
            <Carousel.Item>
                  <img
                        className="d-block w-100 mx-auto"
                        src="/p-1.png"
                        alt="First slide"
                  />
                  <Carousel.Caption>
                        <a href="https://jomar28.gitlab.io/capstone1-francisco/" className="text-decoration-none text-dark" target="_blank">
                              <div className="as-one">
                                    <h3>Static Web App</h3>
                                    <p>First bootcamp project. Technologies: HTML, CSS, Bootstrap.</p>
                              </div>
                        </a>
                  </Carousel.Caption>
            </Carousel.Item>
      */

      return (
            <>
                  <p className="marquee">Git&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GitLab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HTML&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CSS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bootstrap&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JavaScript&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MongoDB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ExpressJS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;React&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NextJS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NodeJS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ChartJS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Photoshop</p>
                  <Container className="projects-page" id="projects">
                        <Row className="p-0">
                              <h2 className="work display-2" data-aos="slide-down" data-aos-offset="0">WORK</h2>
                              <Col className="carousel d-block mx-auto">
                                    <Carousel data-aos="zoom-in" data-aos-offset="200">
                                    <Carousel.Item>
                                          <img
                                                className="d-block w-100 mx-auto"
                                                src="/p-2.png"
                                                alt="Second slide"
                                          />

                                          <Carousel.Caption>
                                                <a href="https://jomar28.gitlab.io/capstone2-francisco/index.html" className="text-decoration-none text-dark" target="_blank">
                                                      <div className="as-one">
                                                            <h3>Course Booking</h3>
                                                            <p>Dynamic Web App. Technologies: HTML, CSS, Bootstrap, MongoDB, JavaScript (including ExpressJS & NodeJS).</p>
                                                      </div>
                                                </a>
                                          </Carousel.Caption>
                                    </Carousel.Item>
                                    <Carousel.Item>
                                          <img
                                                className="d-block w-100 mx-auto"
                                                src="/p-3.png"
                                                alt="Third slide"
                                          />

                                          <Carousel.Caption>
                                                <a href="https://capstone-3-client-francisco-jomar28.vercel.app/" className="text-decoration-none text-dark" target="_blank">
                                                      <div className="as-one">
                                                            <h3>Budget Tracker</h3>
                                                            <p>MERN stack app with Google login & email-sending features.</p>
                                                      </div>
                                                </a>
                                          </Carousel.Caption>
                                    </Carousel.Item>
                              </Carousel>
                        </Col>
                  </Row>
            </Container>
            </>


      )

}