import { useEffect } from 'react'
import {Container,Row,Col} from 'react-bootstrap'
import AOS from 'aos'
import 'aos/dist/aos.css';
import Image from 'next/image'

export default function Footer(){

      useEffect(() => {
            AOS.init({
                  offset: 200,
                  duration: 900
            })
      }, [])

      return(
            <Container>
                  <Row>
                        <Col className="about-text offset-md-2" md={3} data-aos="slide-right" id="about">
                              <p className="font-weight-bold about-title">About</p>
                              <p>Last March 19, I completed a Full Stack Web Development Bootcamp in Zuitt. We were taught about the basics of web development. Although my expectations were met, I realized after the program that there's still sooooo much more to learn. To be honest, it's daunting but coding is fun. The thought of pursuing this track really excites me.</p>
                        </Col>
                        <Col md={5} className="connect" data-aos="slide-left">
                              <h1 className="display-1 font-weight-bold connect-text">&lt;&gt;Let's <span>connect</span>&lt;/&gt;</h1>
                        </Col>
                  </Row>
                  <Row className="footer p-0">
                        <Col className="footer-element offset-md-5 my-5 text-center" md={3}>
                              <p className="fe-1">&copy;2021 Jomar Francisco</p>
                              <p>jomarfrancisco28@gmail.com</p>
                              <a href="https://www.linkedin.com/in/jomarfrancisco/" target="_blank">
                                    <Image
                                          src="/linkedin.png"
                                          width={40}
                                          height={40}
                                    />
                              </a>
                        </Col>    
                  </Row>
            </Container>


      )

}