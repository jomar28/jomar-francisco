import {Container} from 'react-bootstrap'
import Profile from './../components/Profile'
import Projects from './../components/Projects'
import Footer from './../components/Footer'
import Head from 'next/head'

export default function Home() {
  return (
    <>
    <Head>
      <title>Jomar</title>
      <link rel="icon" type="image/png" href="/favicon1.png" />
    </Head>
    <Container>
        <Profile />
        <Projects />
        <Footer />
    </Container>
    </>
  )
}
