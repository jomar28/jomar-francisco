import 'bootstrap/dist/css/bootstrap.min.css'
import { Container,Col } from 'react-bootstrap'
import '../styles/globals.css'
import Image from 'next/image'

function MyApp({ Component, pageProps }) {
	return (
		<>
		<a href="#projects" className="projects ml-5 text-decoration-none">p r o j e c t s</a>
		<Col className="offset-md-11"><a href="#about" className="about ml-5 text-decoration-none">a b o u t</a></Col>		
		<Col className="offset-md-10"><a href="#" className="arrow-up">
				<Image
					src="/arrow-up.svg"
					width={25}
					height={25}
				/>
			</a></Col>		
		<Component {...pageProps} />
		</>
	)

}

export default MyApp
